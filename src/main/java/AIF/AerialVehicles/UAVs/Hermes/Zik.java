package AIF.AerialVehicles.UAVs.Hermes;

import AIF.Missions.*;

public class Zik extends Hermes {
    public Zik(String cameraType, String sensorType, String pilotName, Mission mission, int hoursOfFlightSinceLastRepair, Boolean readyToFly) {
        super(cameraType, sensorType, pilotName, mission, hoursOfFlightSinceLastRepair, readyToFly);
    }

    @Override
    public void setMission(Mission mission) throws MissionTypeException{
        if (mission instanceof IntelligenceMission || mission instanceof BdaMission) {
            this.mission = mission;
        }
        else{
            throw new MissionTypeException("this vehicle cannot preform this mission");
        }
    }
}
